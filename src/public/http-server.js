const http = require("http");

const fs = require("fs");
const path = require("path");
const { wonTossAndMatch } = require("../server/ipl");

const server = http.createServer((req, res) => {
  try {
    if (req.url === "/html" && req.method === "GET") {
      fs.readFile("../public/index.html", "utf-8", (err, data) => {
        if (err) {
          res.end(err);
        } else {
          res.writeHead(200, { "Content-Type": "text/html" });
          res.end(data);
        }
      });
    }
    if (req.url === "/app.js" && req.method === "GET") {
      fs.readFile("../public/app.js", "utf-8", (err, data) => {
        if (err) {
          res.end(err);
        } else {
          res.writeHead(200, { "Content-Type": "text/javascript" });
          res.end(data);
        }
      });
    }
    if (req.url === "/style.css" && req.method === "GET") {
      fs.readFile("../public/style.css", "utf-8", (err, data) => {
        if (err) {
          res.end(err);
        } else {
          res.writeHead(200, { "Content-Type": "text/css" });
          res.end(data);
          console.log(data);
        }
      });
    }
    if (req.url.includes(".json")) {
      fs.readFile(
        path.join(__dirname, "output", req.url),
        "utf-8",
        (err, data) => {
          if (err) {
            console.log(req.url);
            res.end(err);
          } else {
            res.writeHead(200, { "Content-Type": "application/json" });
            res.end(data);
          }
        }
      );
    }
  } catch (err) {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end(404);
  }
});
server.listen(8080, () => {
  console.log("Server listening on port: 8080");
});
