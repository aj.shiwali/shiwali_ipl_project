function fetchAndVisualizationDataMatchesPerYear() {
  fetch("./matchesPerYear.json")
    .then((responce) => responce.json())
    .then(visualizationMatchesPerYear)
    .catch((err) => console.log(err));
}
fetchAndVisualizationDataMatchesPerYear();

function visualizationMatchesPerYear(data) {
  console.log(data);
  let arr = [];
  for (let key in data) {
    arr.push({ name: key, y: data[key] });
  }
  Highcharts.chart("matchesPerYear", {
    chart: {
      type: "column",
    },
    title: {
      text: "1. Number of matches played per year for all the years in IPL.",
    },
    subtitle: {
      text: "Source: WorldClimate.com",
    },
    xAxis: {
      type: "category",
      crosshair: true,
    },
    yAxis: {
      min: 0,
      title: {
        text: "Matches played per year",
      },
    },
    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat:
        '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> of total<br/>',
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        dataLabels: {
          enabled: true,
          format: "{point.y:.0f}",
        },
      },
    },
    series: [
      {
        name: "Match",
        colorByPoint: true,
        data: arr,
      },
    ],
  });
}

function fetchAndVisualizationDataMatchesWonPerTeam() {
  fetch("./matchesWonPerTeam.json")
    .then((responce) => responce.json())
    .then(visualizationMatchesWonPerTeam)
    .catch((err) => console.log(err));
}
fetchAndVisualizationDataMatchesWonPerTeam();

function visualizationMatchesWonPerTeam(data) {
  console.log(data);
  let teams = Object.keys(data);
  //console.log(teams);
  let series = [];
  let distinctSeasons = [];
  for (team in data) {
    for (season in data[team]) {
      if (!distinctSeasons.find((ses) => ses === season)) {
        distinctSeasons.push(season);
      }
    }
  }
  distinctSeasons.sort();
  //console.log(distinctSeasons);
  distinctSeasons.forEach((season) => {
    update(season);
  });
  function update(season) {
    let tempObj = {};
    tempObj["name"] = season;
    tempObj["data"] = [];
    teams.forEach((team) => {
      if (data[team].hasOwnProperty(season)) {
        tempObj.data.push(data[team][season]);
      } else {
        tempObj.data.push(0);
      }
    });
    series.push(tempObj);
  }
  Highcharts.chart("matchesWonPerTeam", {
    chart: {
      type: "column",
    },
    title: {
      text: "2. Number of matches won per team per year in IPL.",
    },
    subtitle: {
      text: "Source: WorldClimate.com",
    },
    xAxis: {
      categories: teams,
    },
    yAxis: {
      min: 0,
      title: {
        text: "Matches won Per Team",
      },
    },
    tooltip: {
      formatter: function () {
        return (
          "<b>" +
          this.x +
          "</b><br/>" +
          this.series.name +
          ": " +
          this.y +
          "<br/>" +
          "Total: " +
          this.point.stackTotal
        );
      },
    },

    plotOptions: {
      column: {
        stacking: "normal",
      },
    },
    series: series,
  });
}

function fetchAndVisualizationDataExtraRunsConceded() {
  fetch("./extraRunsConceded.json")
    .then((responce) => responce.json())
    .then(visualizationExtraRunsConceded)
    .catch((err) => console.log(err));
}
fetchAndVisualizationDataExtraRunsConceded();

function visualizationExtraRunsConceded(data) {
  console.log(data);
  let arr = [];
  for (let key in data) {
    arr.push({ name: key, y: data[key] });
  }
  Highcharts.chart("extraRunsConceded", {
    chart: {
      type: "column",
    },
    title: {
      text: "3. Extra runs conceded per team in the year 2016",
    },
    subtitle: {
      text: "Source: WorldClimate.com",
    },
    xAxis: {
      type: "category",
      crosshair: true,
    },
    yAxis: {
      min: 0,
      title: {
        text: "Extra Runs Conceded Per Team",
      },
    },
    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat:
        '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> of total<br/>',
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        dataLabels: {
          enabled: true,
          format: "{point.y:.0f}",
        },
      },
    },
    series: [
      {
        name: "Extra Runs",
        colorByPoint: true,
        data: arr,
      },
    ],
  });
}

function fetchAndVisualizationWonTossAndMatch() {
  fetch("./wonTossAndMatch.json")
    .then((responce) => responce.json())
    .then(visualizationWonTossAndMatch)
    .catch((err) => console.log(err));
}
fetchAndVisualizationWonTossAndMatch();

function visualizationWonTossAndMatch(data) {
  console.log(data);
  let arr = [];
  for (let key in data) {
    arr.push({ name: key, y: data[key] });
  }
  Highcharts.chart("wonTossAndMatch", {
    chart: {
      type: "column",
    },
    title: {
      text: "4. Number of times each team won the toss and also won the match.",
    },
    subtitle: {
      text: "Source: WorldClimate.com",
    },
    xAxis: {
      type: "category",
      crosshair: true,
    },
    yAxis: {
      min: 0,
      title: {
        text: "Number of times team won match and toss both",
      },
    },
    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat:
        '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> of total<br/>',
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        dataLabels: {
          enabled: true,
          format: "{point.y:.0f}",
        },
      },
    },
    series: [
      {
        name: "Teams",
        colorByPoint: true,
        data: arr,
      },
    ],
  });
}

function topTenEconomicalBowler() {
  //console.log(matchPerYearData);
  fetch("./economicBowlers.json")
    .then((response) => response.json())
    .then((datas) => {
      console.log(datas);
      Highcharts.chart("topTenEconomicBowler", {
        chart: {
          type: "column",
        },
        title: {
          text: "Top 10 economical bowlers in the year 2015",
        },
        subtitle: {
          text: 'Source: <a href="https://www.kaggle.com" target="_blank">kaggle</a>',
        },
        xAxis: {
          type: "category",
          labels: {
            rotation: -45,
            style: {
              fontSize: "13px",
              fontFamily: "Verdana, sans-serif",
            },
          },
        },
        yAxis: {
          min: 0,
          title: {
            text: "Economical bowler (year 2015)",
          },
        },
        legend: {
          enabled: false,
        },
        tooltip: {
          pointFormat: "Economical bowler: <b>{point.y:.1f} year 2015</b>",
        },
        series: [
          {
            name: "Matches",
            data: datas,
            dataLabels: {
              enabled: true,
              rotation: -90,
              color: "#FFFFFF",
              align: "right",
              format: "{point.y:.1f}", // one decimal
              y: 10, // 10 pixels down from the top
              style: {
                fontSize: "13px",
                fontFamily: "Verdana, sans-serif",
              },
            },
          },
        ],
      });
    })
    .catch((err) => console.log(err));
}
topTenEconomicalBowler();
