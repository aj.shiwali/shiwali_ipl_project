// For output files
const path = require("path");
const matches = require(path.join(__dirname, "../data/matches.json"));
const deliveries = require(path.join(__dirname, "../data/deliveries.json"));
const {
  matchesPerYear,
  matchesWonPerTeam,
  extraRunsConceded,
  economicalBowlers,
  wonTossAndMatch,
  playerOfTheMatch,
  strikeRate,
  playerDismissed,
  bestEconomy,
} = require("./ipl");

const fs = require("fs");
fs.writeFileSync(
  "../public/output/matchesPerYear.json",
  JSON.stringify(matchesPerYear(matches)),
  null,
  "\n"
);

fs.writeFileSync(
  "../public/output/matchesWonPerTeam.json",
  JSON.stringify(matchesWonPerTeam(matches)),
  null,
  "\n"
);

fs.writeFileSync(
  "../public/output/extraRunsConceded.json",
  JSON.stringify(extraRunsConceded(matches, deliveries)),
  null,
  "\n"
);

fs.writeFileSync(
  "../public/output/economicBowlers.json",
  JSON.stringify(economicalBowlers(matches, deliveries)),
  null,
  "\n"
);
fs.writeFileSync(
  "../public/output/wonTossAndMatch.json",
  JSON.stringify(wonTossAndMatch(matches)),
  null,
  "\n"
);
fs.writeFileSync(
  "../public/output/strikeRate.json",
  JSON.stringify(strikeRate(matches, deliveries, "MS Dhoni")),
  null,
  "\n"
);
fs.writeFileSync(
  "../public/output/playerDismissed.json",
  JSON.stringify(playerDismissed(deliveries)),
  null,
  "\n"
);
fs.writeFileSync(
  "../public/output/bestEconomy.json",
  JSON.stringify(bestEconomy(deliveries)),
  null,
  "\n"
);
