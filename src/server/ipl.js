// Problem 1 - Number of matches played per year for all the years in IPL

exports.matchesPerYear = (matchDetails) => {
  let numberOfMatches = {};
  for (let i = 0; i < matchDetails.length; i++) {
    if (numberOfMatches[matchDetails[i].season]) {
      numberOfMatches[matchDetails[i].season] += 1;
    } else {
      numberOfMatches[matchDetails[i].season] = 1;
    }
  }
  return numberOfMatches;
};

// Problem 2 - Number of matches won per team per year in IPL.

exports.matchesWonPerTeam = (matchDetails) => {
  let winsPerTeam = {};
  for (let i = 0; i < matchDetails.length; i++) {
    if (winsPerTeam[matchDetails[i].winner]) {
      if (winsPerTeam[matchDetails[i].winner][matchDetails[i].season]) {
        winsPerTeam[matchDetails[i].winner][matchDetails[i].season] += 1;
      } else {
        winsPerTeam[matchDetails[i].winner][matchDetails[i].season] = 1;
      }
    } else {
      winsPerTeam[matchDetails[i].winner] = {};
      winsPerTeam[matchDetails[i].winner][matchDetails[i].season] = 1;
    }
  }
  return winsPerTeam;
};

// Problem 3 - Extra runs conceded per team in the year 2016

exports.extraRunsConceded = (matchDetails, deliveryDetails) => {
  let extraRuns = {};
  for (let i = 0; i < matchDetails.length; i++) {
    if (matchDetails[i].season === "2016") {
      for (let j = 0; j < deliveryDetails.length; j++) {
        if (deliveryDetails[j].match_id === matchDetails[i].id) {
          if (extraRuns[deliveryDetails[j].bowling_team]) {
            extraRuns[deliveryDetails[j].bowling_team] += parseInt(
              deliveryDetails[j].extra_runs
            );
          } else {
            extraRuns[deliveryDetails[j].bowling_team] = parseInt(
              deliveryDetails[j].extra_runs
            );
          }
        }
      }
    }
  }
  return extraRuns;
};

// Problem - 4 Top 10 economical bowlers in the year 2015

exports.economicalBowlers = (match, delivery) => {
  let economicBowler = {};
  let bowlers = {};
  for (let i = 0; i < match.length; i++) {
    if (match[i].season === "2015") {
      for (let j = 0; j < delivery.length; j++) {
        if (delivery[j].match_id === match[i].id) {
          if (bowlers[delivery[j].bowler]) {
            if (delivery[j].ball === "1") {
              bowlers[delivery[j].bowler]["overs"] += 1;
            }
            bowlers[delivery[j].bowler].runs += parseInt(
              delivery[j].total_runs
            );
            if (bowlers[delivery[j].bowler].runs > 0) {
              bowlers[delivery[j].bowler].economy = (
                bowlers[delivery[j].bowler].runs /
                bowlers[delivery[j].bowler].overs
              ).toFixed(1);
            }
          } else {
            bowlers[delivery[j].bowler] = {
              runs: parseInt(delivery[j].total_runs),
              overs: 1,
              economy: parseInt(delivery[j].total_runs),
            };
          }
        }
      }
    }
  }
  let sortedBowlers = [];
  for (let key in bowlers) {
    sortedBowlers.push([key, bowlers[key]["economy"]]);
  }

  sortedBowlers.sort(function (a, b) {
    return a[1] - b[1];
  });
  for (let i = 0; i < 10; i++) {
    economicBowler[sortedBowlers[i][0]] = sortedBowlers[i][1];
  }
  return economicBowler;
};

// Additional Problem set

// number of times each team won the toss and also won the match

exports.wonTossAndMatch = (matches) => {
  let winTimes = {};
  for (let index = 0; index < matches.length; index++) {
    if (matches[index].winner === matches[index]["toss_winner"]) {
      if (winTimes[matches[index].winner]) {
        winTimes[matches[index].winner] += 1;
      } else {
        winTimes[matches[index].winner] = 1;
      }
    }
  }
  return winTimes;
};

//  player who has won the highest number of Player of the Match awards for each season

exports.playerOfTheMatch = (object) => {
  let players = {};
  let playersPerSeason = {};
  for (let index = 0; index < object.length; index++) {
    if (playersPerSeason[object[index].season]) {
      if (
        playersPerSeason[object[index].season][object[index]["player_of_match"]]
      ) {
        playersPerSeason[object[index].season][
          object[index]["player_of_match"]
        ] += 1;
      } else {
        playersPerSeason[object[index].season][
          object[index]["player_of_match"]
        ] = 1;
      }
    } else {
      playersPerSeason[object[index].season] = {};
      playersPerSeason[object[index].season][
        object[index]["player_of_match"]
      ] = 1;
    }
  }
  for (let season in playersPerSeason) {
    players[season] = [];
    for (let player in playersPerSeason[season]) {
      players[season].push([player, playersPerSeason[season][player]]);
    }
    players[season].sort((firstValue, secondValue) => {
      return secondValue[1] - firstValue[1];
    });
    playersPerSeason[season] = {
      name: players[season][0][0],
      matches: players[season][0][1],
    };
  }
  return playersPerSeason;
};

// strike rate of a batsman for each season

exports.strikeRate = (matches, deliveries, givenPlayer) => {
  let strikeRateOfBatsman = {};
  let calculateStrikeRate = {};
  for (let index = 0; index < matches.length; index++) {
    if (!calculateStrikeRate[matches[index].season]) {
      calculateStrikeRate[matches[index].season] = {};
    }
    for (let j = 0; j < deliveries.length; j++) {
      if (deliveries[j].match_id === matches[index].id) {
        if (calculateStrikeRate[matches[index].season][deliveries[j].batsman]) {
          calculateStrikeRate[matches[index].season][
            deliveries[j].batsman
          ].runs += parseInt(deliveries[j]["total_runs"]);
          calculateStrikeRate[matches[index].season][
            deliveries[j].batsman
          ].balls += 1;
        } else {
          calculateStrikeRate[matches[index].season][deliveries[j].batsman] = {
            runs: parseInt(deliveries[j]["total_runs"]),
            balls: 1,
          };
        }
      }
    }
  }
  for (let season in calculateStrikeRate) {
    strikeRateOfBatsman[season] = {};
    for (let batsman in calculateStrikeRate[season]) {
      strikeRateOfBatsman[season][batsman] =
        (calculateStrikeRate[season][batsman].runs /
          calculateStrikeRate[season][batsman].balls) *
        100;
    }
  }
  let strikeRateOfGivenBatsman = {};
  strikeRateOfGivenBatsman[givenPlayer] = {};
  for (let season in strikeRateOfBatsman) {
    if (strikeRateOfBatsman[season][givenPlayer]) {
      strikeRateOfGivenBatsman[givenPlayer][season] =
        strikeRateOfBatsman[season][givenPlayer];
    } else {
      strikeRateOfGivenBatsman[givenPlayer][season] = 0;
    }
  }

  return strikeRateOfGivenBatsman;
};

//highest number of times one player has been dismissed by another player
exports.playerDismissed = (deliveries) => {
  let numberOfTimes = {};

  for (let index = 0; index < deliveries.length; index++) {
    if (
      deliveries[index]["player_dismissed"] &&
      deliveries[index]["dismissal_kind"] != "retired hurt" &&
      deliveries[index]["dismissal_kind"] != "obstructing the field"
    ) {
      if (numberOfTimes[deliveries[index]["player_dismissed"]]) {
        if (deliveries[index]["feilder"]) {
          if (
            numberOfTimes[deliveries[index]["player_dismissed"]][
              deliveries[index]["fielder"]
            ]
          ) {
            numberOfTimes[deliveries[index]["player_dismissed"]][
              deliveries[index]["fielder"]
            ] += 1;
          } else {
            numberOfTimes[deliveries[index]["player_dismissed"]][
              deliveries[index]["fielder"]
            ] = 1;
          }
        } else {
          if (
            numberOfTimes[deliveries[index]["player_dismissed"]][
              deliveries[index]["bowler"]
            ]
          ) {
            numberOfTimes[deliveries[index]["player_dismissed"]][
              deliveries[index]["bowler"]
            ] += 1;
          } else {
            numberOfTimes[deliveries[index]["player_dismissed"]][
              deliveries[index]["bowler"]
            ] = 1;
          }
        }
      } else {
        numberOfTimes[deliveries[index]["player_dismissed"]] = {};
        if (deliveries[index]["fielder"]) {
          numberOfTimes[deliveries[index]["player_dismissed"]][
            deliveries[index]["fielder"]
          ] = 1;
        } else {
          numberOfTimes[deliveries[index]["player_dismissed"]][
            deliveries[index]["bowler"]
          ] = 1;
        }
      }
    }
  }

  let max = 0;
  let batsman;
  for (const player in numberOfTimes) {
    let array = Object.entries(numberOfTimes[player]);
    for (let index = 0; index < array.length; index++) {
      if (max < array[index][1]) {
        max = array[index][1];
        batsman = player;
      }
    }
  }

  return {
    name: batsman,
    dismissedTimes: max,
  };
};

//bowler with the best economy in super overs

exports.bestEconomy = (deliveries) => {
  let bestEconomyBowler = {};
  let bowlers = {};
  for (let index = 0; index < deliveries.length; index++) {
    if (deliveries[index]["is_super_over"] === "1") {
      if (bowlers[deliveries[index].bowler]) {
        if (
          bowlers[deliveries[index].bowler].matchId !==
          deliveries[index]["match_id"]
        ) {
          bowlers[deliveries[index].bowler].matchId =
            deliveries[index]["match_id"];
          bowlers[deliveries[index].bowler].overs += 1;
        }
        bowlers[deliveries[index].bowler].runs += parseInt(
          deliveries[index]["total_runs"]
        );
      } else {
        bowlers[deliveries[index].bowler] = {
          runs: parseInt(deliveries[index]["total_runs"]),
          overs: 1,
          matchId: deliveries[index]["match_id"],
        };
      }
    }
  }
  let maxNumber = Number.MAX_VALUE;
  for (const player in bowlers) {
    bowlers[player]["economy"] = bowlers[player].runs / bowlers[player].overs;
    delete bowlers[player].matchId;
    if (bowlers[player]["economy"] < maxNumber) {
      maxNumber = bowlers[player]["economy"];
    }
  }
  for (const player in bowlers) {
    if (bowlers[player].economy === maxNumber) {
      bestEconomyBowler[player] = bowlers[player];
    }
  }
  return bestEconomyBowler;
};
